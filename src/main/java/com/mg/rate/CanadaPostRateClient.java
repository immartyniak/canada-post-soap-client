package com.mg.rate;

public interface CanadaPostRateClient {
  RateResponse rate(RateRequest request);
}
