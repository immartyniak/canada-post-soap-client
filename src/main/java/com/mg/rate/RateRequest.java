package com.mg.rate;

import com.mg.rate.webservices.mailingscenario.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class RateRequest {

    private String username;
    private String password;

    private Locale locale = Locale.ENGLISH;
    private String platformId;

    private MailingScenario mailingScenario = new MailingScenario();

    public RateRequest() {
        username = "0185703c305d4afb";
        password = "372fecfc3a65b39dda747e";
        getMailingScenario().setCustomerNumber("8354711");


        getMailingScenario().setOriginPostalCode("K2B8J6");

        getMailingScenario().getDestination().setPostalCode("J0E1X0");
        getMailingScenario().getParcelCharacteristics().setWeight(new BigDecimal(1));


    }

    public Locale getLocale() {
        return locale;
    }

    public void setLocale(Locale locale) {
        this.locale = locale;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public MailingScenario getMailingScenario() {
        return mailingScenario;
    }

    public void setMailingScenario(MailingScenario mailingScenario) {
        this.mailingScenario = mailingScenario;
    }

    public String getPlatformId() {
        return platformId;
    }

    public void setPlatformId(String platformId) {
        this.platformId = platformId;
    }
}



