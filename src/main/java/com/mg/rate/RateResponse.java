package com.mg.rate;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class RateResponse {

    public static class Rate {
        String serviceName;
        BigDecimal price;

        public Rate(String serviceName, BigDecimal price) {
            this.serviceName = serviceName;
            this.price = price;
        }

        @Override
        public String toString() {
            return "Service name " + serviceName + " Price: " + price;
        }
    }

    private List<Rate> rates = new ArrayList<Rate>();

    public void add(String serviceName, BigDecimal price) {
        add(new Rate(serviceName, price));
    }

    public void add(Rate rate) {
        rates.add(rate);
    }

    public List<Rate> getRates() {
        return rates;
    }
}
