package com.mg.rate.webservices;

import ca.canadapost.ws.soap.ship.rate.v3.GetRatesRequest;
import ca.canadapost.ws.soap.ship.rate.v3.GetRatesResponse;
import ca.canadapost.ws.soap.ship.rate.v3.RatingPortType;
import ca.canadapost.ws.soap.ship.rate.v3.RatingService;
import com.mg.rate.CanadaPostRateClient;
import com.mg.rate.RateRequest;
import com.mg.rate.RateResponse;
import org.apache.cxf.frontend.ClientProxy;
import org.apache.cxf.ws.security.wss4j.WSS4JOutInterceptor;
import org.apache.ws.security.WSConstants;
import org.apache.ws.security.WSPasswordCallback;
import org.apache.ws.security.handler.WSHandlerConstants;

import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.UnsupportedCallbackException;
import javax.xml.namespace.QName;
import javax.xml.ws.BindingProvider;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class RateClientImpl implements CanadaPostRateClient {

    public static class PasswordCallback implements CallbackHandler {
        private final String password;

        private PasswordCallback(String password) {
            this.password = password;
        }

        public void handle(Callback[] callbacks) throws IOException, UnsupportedCallbackException {
            WSPasswordCallback pc = (WSPasswordCallback) callbacks[0];
            pc.setPassword(password);
        }
    }


    @Override
    public RateResponse rate(RateRequest request) {
        RatingService service = new RatingService(null, new QName("http://www.canadapost.ca/ws/soap/ship/rate/v3", "RatingService"));
        RatingPortType port = service.getRatingPort();

        ((BindingProvider) port).getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, "https://soa-gw.canadapost.ca/rs/soap/rating/v3");

        //wss4j interceptor -  add UsernameToken to soap header
        ClientProxy.getClient(port).getOutInterceptors().add(getUsernameTokenInterceptor(request.getUsername(), request.getPassword()));

        GetRatesRequest ratesRequest = CreateRequestTranslator.from(request);
        GetRatesResponse response = port.getRates(ratesRequest);

        return CreateRateResponseTranslator.from(response);
    }

    public static WSS4JOutInterceptor getUsernameTokenInterceptor(String username, String password) {
        Map props = new HashMap();
        props.put(WSHandlerConstants.ACTION, WSHandlerConstants.USERNAME_TOKEN);
        props.put(WSHandlerConstants.USER, username);
        props.put(WSHandlerConstants.PASSWORD_TYPE, WSConstants.PW_TEXT);
        props.put(WSHandlerConstants.PW_CALLBACK_REF, new PasswordCallback(password));

        return new WSS4JOutInterceptor(props);
    }


}
