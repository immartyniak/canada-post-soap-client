package com.mg.rate.webservices;

import ca.canadapost.ws.soap.ship.rate.v3.GetRatesResponse;
import ca.canadapost.ws.soap.ship.rate.v3.Messages;
import com.mg.rate.RateResponse;
import com.mg.rate.RateResponse.Rate;

public class CreateRateResponseTranslator {

    public static RateResponse from(GetRatesResponse response) {
        RateResponse rateResponse = new RateResponse();

        if (response.getMessages() == null) {
            if (response.getPriceQuotes() != null) {
                for (GetRatesResponse.PriceQuotes.PriceQuote priceQuote : response.getPriceQuotes().getPriceQuote()) {
                    Rate rate = new Rate(priceQuote.getServiceName(), priceQuote.getPriceDetails().getDue());
                    rateResponse.add(rate);
                    System.out.println(rate);
                }
            }
        } else {
            // Assume Error
            for (Messages.Message m : response.getMessages().getMessage()) {
                System.out.println("Error Code: " + m.getCode());
                System.out.println("Error Message: " + m.getDescription());
            }
        }

        return rateResponse;
    }
}
