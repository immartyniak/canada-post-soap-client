package com.mg.rate.webservices;

import ca.canadapost.ws.soap.ship.rate.v3.GetRatesRequest;
import com.mg.rate.RateRequest;
import com.mg.rate.webservices.mailingscenario.*;

import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

public class CreateRequestTranslator {

    public static GetRatesRequest from(RateRequest request) {
        GetRatesRequest soapRequest = new GetRatesRequest();
        soapRequest.setPlatformId(request.getPlatformId());
        soapRequest.setLocale(request.getLocale().getLanguage());


        GetRatesRequest.MailingScenario.ParcelCharacteristics parcelCharacteristics = new GetRatesRequest.MailingScenario.ParcelCharacteristics();
        parcelCharacteristics.setWeight(request.getMailingScenario().getParcelCharacteristics().getWeight());

        GetRatesRequest.MailingScenario.Destination.Domestic domestic = new GetRatesRequest.MailingScenario.Destination.Domestic();
        domestic.setPostalCode(request.getMailingScenario().getDestination().getPostalCode());

        GetRatesRequest.MailingScenario.Destination destination = new GetRatesRequest.MailingScenario.Destination();
        destination.setDomestic(domestic);

        GetRatesRequest.MailingScenario mailingScenario = new GetRatesRequest.MailingScenario();
        mailingScenario.setCustomerNumber(request.getMailingScenario().getCustomerNumber());
        mailingScenario.setParcelCharacteristics(parcelCharacteristics);
        mailingScenario.setOriginPostalCode(request.getMailingScenario().getOriginPostalCode());
        mailingScenario.setDestination(destination);

        soapRequest.setMailingScenario(mailingScenario);


        setSoapMailingScenario(request.getMailingScenario());

        return soapRequest;
    }

    private static void setSoapMailingScenario(MailingScenario mailingScenario) {
        GetRatesRequest.MailingScenario soapMailingScenario = new GetRatesRequest.MailingScenario();
        soapMailingScenario.setCustomerNumber(mailingScenario.getCustomerNumber());
        soapMailingScenario.setContractId(mailingScenario.getContractId());
        soapMailingScenario.setPromoCode(mailingScenario.getPromoCode());
        soapMailingScenario.setQuoteType(mailingScenario.getQuoteType().getQuoteTypeText());
        soapMailingScenario.setExpectedMailingDate(dateToXMLGregorianCalendar(mailingScenario.getExpectedMilingDate()));
        soapMailingScenario.setOptions(getSoapOprions(mailingScenario.getOptions()));
        soapMailingScenario.setParcelCharacteristics(getSoapParcelCharacteristics(mailingScenario.getParcelCharacteristics()));
        soapMailingScenario.setServices(getSoapServices(mailingScenario.getServices()));
        soapMailingScenario.setOriginPostalCode(mailingScenario.getOriginPostalCode());
        soapMailingScenario.setDestination(getSoapDestination(mailingScenario.getDestination()));
    }

    private static GetRatesRequest.MailingScenario.Destination getSoapDestination(Destination destination) {
        GetRatesRequest.MailingScenario.Destination soapDestination = new GetRatesRequest.MailingScenario.Destination();

        if (destination.getPostalCode() != null) {
            GetRatesRequest.MailingScenario.Destination.Domestic domestic = new GetRatesRequest.MailingScenario.Destination.Domestic();
            domestic.setPostalCode(destination.getPostalCode());
            soapDestination.setDomestic(domestic);
        }

        return soapDestination;
    }

    private static GetRatesRequest.MailingScenario.Services getSoapServices(List<ServiceCode> services) {
        GetRatesRequest.MailingScenario.Services soapServices = new GetRatesRequest.MailingScenario.Services();
        services.forEach(service -> {
            soapServices.getServiceCode().add(service.getCode());
        });
        return soapServices;
    }

    private static GetRatesRequest.MailingScenario.ParcelCharacteristics getSoapParcelCharacteristics(ParcelCharacteristics parcelCharacteristics) {
        GetRatesRequest.MailingScenario.ParcelCharacteristics soapParcelCharacteristics1 = new GetRatesRequest.MailingScenario.ParcelCharacteristics();
        soapParcelCharacteristics1.setWeight(parcelCharacteristics.getWeight());

        GetRatesRequest.MailingScenario.ParcelCharacteristics.Dimensions dimensions = new GetRatesRequest.MailingScenario.ParcelCharacteristics.Dimensions();
        dimensions.setHeight(parcelCharacteristics.getDimensions().getHeight());
        dimensions.setLength(parcelCharacteristics.getDimensions().getLength());
        dimensions.setWidth(parcelCharacteristics.getDimensions().getWidth());
        soapParcelCharacteristics1.setDimensions(dimensions);

        soapParcelCharacteristics1.setUnpackaged(parcelCharacteristics.getUnpackaged());
        soapParcelCharacteristics1.setMailingTube(parcelCharacteristics.getMailingTube());
        soapParcelCharacteristics1.setOversized(parcelCharacteristics.getOversized());
        return soapParcelCharacteristics1;
    }

    private static GetRatesRequest.MailingScenario.Options getSoapOprions(List<Option> options) {
        GetRatesRequest.MailingScenario.Options soapOptions = new GetRatesRequest.MailingScenario.Options();

        options.forEach(option -> {
            GetRatesRequest.MailingScenario.Options.Option soapOption = new GetRatesRequest.MailingScenario.Options.Option();
            soapOption.setOptionAmount(option.getOptionAmount());
            soapOption.setOptionCode(option.getOptionCode());
            soapOption.setComment(option.getComment());
            soapOptions.getOption().add(soapOption);
        });

        return soapOptions;
    }

    private static void setParcelCharacteristics(GetRatesRequest.MailingScenario mailingScenario, RateRequest request) {

    }

    public static XMLGregorianCalendar dateToXMLGregorianCalendar(Date date) {
        if (date == null) {
            return null;
        }

        XMLGregorianCalendar xmlGregorianCalendar = null;
        GregorianCalendar gregorianCalendar = new GregorianCalendar();
        gregorianCalendar.setTime(date);
//        gregorianCalendar.setTimeZone(zone);
        try {
            DatatypeFactory dataTypeFactory = DatatypeFactory.newInstance();
            xmlGregorianCalendar = dataTypeFactory.newXMLGregorianCalendar(gregorianCalendar);
        } catch (Exception e) {
            System.out.println("Exception in conversion of Date to XMLGregorianCalendar" + e);
        }

        return xmlGregorianCalendar;
    }
}
