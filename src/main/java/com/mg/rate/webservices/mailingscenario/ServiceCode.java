package com.mg.rate.webservices.mailingscenario;

public enum ServiceCode {
    DOM_RP("DOM.RP", "Regular Parcel"),
    DOM_EP("DOM.EP", "Expedited Parcel"),
    DOM_XP("DOM.XP", "Xpresspost"),
    DOM_XP_CERT("DOM.XP.CERT", "Xpresspost Certified"),
    DOM_PC("DOM.PC", "Priority"),
    DOM_LIB("DOM.LIB", "Library Materials"),
    USA_EP("USA.EP", "Expedited Parcel USA"),
    USA_PW_ENV("USA.PW.ENV", "Priority Worldwide Envelope USA"),
    USA_PW_PAK("USA.PW.PAK", "Priority Worldwide pak USA"),
    USA_PW_PARCEL("USA.PW.PARCEL", "Priority Worldwide Parcel USA"),
    USA_SP_AIR("USA.SP.AIR", "Small Packet USA Air"),
    USA_TP("USA.TP", "Tracked Packet – USA"),
    USA_TP_LVM("USA.TP.LVM", "Tracked Packet – USA (LVM) (large volume mailers)"),
    USA_XP("USA.XP", "Xpresspost USA"),
    INT_XP("INT.XP", "Xpresspost International"),
    INT_IP_AIR("INT.IP.AIR", "International Parcel Air"),
    INT_IP_SURF("INT.IP.SURF", "International Parcel Surface"),
    INT_PW_ENV("INT.PW.ENV", "Priority Worldwide Envelope Int’l"),
    INT_PW_PAK("INT.PW.PAK", "Priority Worldwide pak Int’l"),
    INT_PW_PARCEL("INT.PW.PARCEL", "Priority Worldwide parcel Int’l"),
    INT_SP_AIR("INT.SP.AIR", "Small Packet International Air"),
    INT_SP_SURF("INT.SP.SURF", "Small Packet International Surface"),
    INT_TP("INT.TP", "Tracked Packet – International");

    String code;
    String description;

    ServiceCode(String code, String description) {
        this.code = code;
        this.description = description;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}