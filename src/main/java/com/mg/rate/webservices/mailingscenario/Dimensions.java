package com.mg.rate.webservices.mailingscenario;

import java.math.BigDecimal;

public class Dimensions {
    private BigDecimal length;
    private BigDecimal width;
    private BigDecimal height;

    public BigDecimal getLength() {
        return length;
    }

    public void setLength(BigDecimal length) {
        this.length = length;
    }

    public BigDecimal getWidth() {
        return width;
    }

    public void setWidth(BigDecimal width) {
        this.width = width;
    }

    public BigDecimal getHeight() {
        return height;
    }

    public void setHeight(BigDecimal height) {
        this.height = height;
    }
}
