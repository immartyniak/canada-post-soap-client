package com.mg.rate.webservices.mailingscenario;

import java.math.BigDecimal;

public class ParcelCharacteristics {
    private BigDecimal weight;
    private Dimensions dimensions = new Dimensions();
    private Boolean unpackaged;
    private Boolean mailingTube;
    private Boolean oversized;

    public Dimensions getDimensions() {
        return dimensions;
    }

    public void setDimensions(Dimensions dimensions) {
        this.dimensions = dimensions;
    }

    public Boolean getUnpackaged() {
        return unpackaged;
    }

    public void setUnpackaged(Boolean unpackaged) {
        this.unpackaged = unpackaged;
    }

    public Boolean getMailingTube() {
        return mailingTube;
    }

    public void setMailingTube(Boolean mailingTube) {
        this.mailingTube = mailingTube;
    }

    public Boolean getOversized() {
        return oversized;
    }

    public void setOversized(Boolean oversized) {
        this.oversized = oversized;
    }

    public BigDecimal getWeight() {
        return weight;
    }

    public void setWeight(BigDecimal weight) {
        this.weight = weight;
    }
}
