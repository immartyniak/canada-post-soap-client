package com.mg.rate.webservices.mailingscenario;

import java.math.BigDecimal;

public class Option {
    private String optionCode;
    private BigDecimal optionAmount;
    private String comment;

    public String getOptionCode() {
        return optionCode;
    }

    public void setOptionCode(String optionCode) {
        this.optionCode = optionCode;
    }

    public BigDecimal getOptionAmount() {
        return optionAmount;
    }

    public void setOptionAmount(BigDecimal optionAmount) {
        this.optionAmount = optionAmount;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
