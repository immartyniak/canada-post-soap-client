package com.mg.rate.webservices.mailingscenario;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MailingScenario {
    private String customerNumber;
    private String contractId = null;
    private String promoCode = null;
    private QuoteType quoteType = QuoteType.COMMERCIAL;
    private Date expectedMilingDate = null;
    private List<Option> options = new ArrayList<Option>();
    private ParcelCharacteristics parcelCharacteristics = new ParcelCharacteristics();
    private List<ServiceCode> services = new ArrayList<ServiceCode>();
    private String originPostalCode;
    private Destination destination = new Destination();

    public String getCustomerNumber() {
        return customerNumber;
    }

    public void setCustomerNumber(String customerNumber) {
        this.customerNumber = customerNumber;
    }

    public String getContractId() {
        return contractId;
    }

    public void setContractId(String contractId) {
        this.contractId = contractId;
    }

    public String getPromoCode() {
        return promoCode;
    }

    public void setPromoCode(String promoCode) {
        this.promoCode = promoCode;
    }

    public QuoteType getQuoteType() {
        return quoteType;
    }

    public void setQuoteType(QuoteType quoteType) {
        this.quoteType = quoteType;
    }

    public Date getExpectedMilingDate() {
        return expectedMilingDate;
    }

    public void setExpectedMilingDate(Date expectedMilingDate) {
        this.expectedMilingDate = expectedMilingDate;
    }

    public List<Option> getOptions() {
        return options;
    }

    public void setOptions(List<Option> options) {
        this.options = options;
    }

    public ParcelCharacteristics getParcelCharacteristics() {
        return parcelCharacteristics;
    }

    public void setParcelCharacteristics(ParcelCharacteristics parcelCharacteristics) {
        this.parcelCharacteristics = parcelCharacteristics;
    }

    public List<ServiceCode> getServices() {
        return services;
    }

    public void setServices(List<ServiceCode> services) {
        this.services = services;
    }

    public String getOriginPostalCode() {
        return originPostalCode;
    }

    public void setOriginPostalCode(String originPostalCode) {
        this.originPostalCode = originPostalCode;
    }

    public Destination getDestination() {
        return destination;
    }

    public void setDestination(Destination destination) {
        this.destination = destination;
    }
}
