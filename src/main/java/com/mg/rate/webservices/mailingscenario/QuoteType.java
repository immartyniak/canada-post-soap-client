package com.mg.rate.webservices.mailingscenario;

public enum QuoteType {
    COMMERCIAL("commercial"),
    COUNTER("counter");

    String quoteTypeText;

    QuoteType(String quoteTypeText) {
        this.quoteTypeText = quoteTypeText;
    }

    public String getQuoteTypeText() {
        return quoteTypeText;
    }
}
