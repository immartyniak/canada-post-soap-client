package com.mg;

import com.mg.rate.CanadaPostRateClient;
import com.mg.rate.CreateRateRequestTranslator;
import com.mg.rate.RateRequest;
import com.mg.rate.RateResponse;

public class CanadaPostFacade {
  private final CanadaPostRateClient rateClient;

  public CanadaPostFacade(CanadaPostRateClient rateClient) {
    this.rateClient = rateClient;
  }

  public void rate() {
    RateRequest request = CreateRateRequestTranslator.from();
    RateResponse response = rateClient.rate(request);
  }
}
