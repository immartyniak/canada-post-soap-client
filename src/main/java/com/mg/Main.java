package com.mg;

import com.mg.rate.webservices.RateClientImpl;

public class Main {

    public static void main(String[] args){
        CanadaPostFacade canadaPostFacade = new CanadaPostFacade(new RateClientImpl());
        canadaPostFacade.rate();
    }
}
